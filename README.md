unextract 
=========

* Removes the extracted archive contents from the current directory
* While removing, verifies the files by size
* Supports ``rar``, ``zip`` and ``tar.gz`` archives

Usage
-----

```
unextract [-u] ARCHIVE...
    -u remove the files even if the sizes are different
```